# Book Store

This repository serves as the base rails repository to complete the Book Store evaluation task.

The Book Store is a simple frontend and backend application that allows backend users / moderators to upload PDF files
and automatically converts them to HTML. The public frontend allows users to browse, filter and view the
converted PDF files.

This evaluation task and development test covers the skillset of a Ruby/Rails and JavaScript developer,
or Full-Stack developer, covering basic Backend functionality, a simple Web-App using Angular.js,
and some background job processing.

The following gems are involved in this task:

* rails: web application server, api-server and background job processing
* sqlite3: database adapter
* webpacker: asset processing engine
* activeadmin: administration interface framework
* grape & grape-entity: api server
* kristin: simple conversion from PDF to HTML
* paperclip: media-file storage and automatic thumbnail generator
* resque: library for creating background jobs using redis

## Installation

First, install the following dependencies:

* brew install sqlite
* brew install redis
* brew install imagemagick
* brew install pdf2htmlEX
* brew install node
* brew install yarn
* Install Ruby v2.3.3 via [rbenv](https://github.com/rbenv/rbenv)

Next, install this app via bundler:

* gem install bundler
* bundle install
* bundle exec rails webpacker:install

Lastly, initialize the development database:

* bundle exec rails db:setup

To run the local development environment via Webrick, run:

* bundle exec rails server

## Starting the Evaluation

Once you have set up your local development environment,
and everything is working fine, please let us know
and we will send you the evaluation briefing.

This evaluation task is divided into 5 steps. In total,
the whole task is intended be achievable within 4 hours.

During the evaluation, we want to avoid having you "stuck"
during any of the steps. Therefore, we practice live-support
during our evaluation tests. If something is unclear, or you
are stuck with a bug or other problem, please reach out to us
and we will assist via call / screencast.

Reaching out and collaborative problem-solving is encouraged,
and doing so will not be rated netagively in our review.
Instead, candidates usually get better overall results by
claiming live-support / peer programming during the course
of this evaluation.

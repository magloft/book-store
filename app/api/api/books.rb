module Api
  class Books < Grape::API
    format :json

    desc 'Retrieve a list of books'
    get do
      present []
    end
  end
end

ActiveAdmin.setup do |config|
  config.site_title = 'Book Store'
  config.root_to = 'books#index'
  config.comments = false
  config.batch_actions = true
  config.localize_format = :long
end

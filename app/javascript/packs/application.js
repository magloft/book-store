/* eslint no-console:0 */

// Vendor Scripts
import 'angular'
import 'angular-material'
import 'angular-resource'

// Stylesheets
import '../app/scss/application'

// App Modules
const app = angular.module('app', ['ngMaterial']) 
